//
//  FPIGeneralConstants.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/8/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIGeneralConstants.h"

NSString * const appGroupIdentifier = @"group.com.DM.fpiPlacetest";
NSString * const sqliteStoreName = @"fpiPlaceDB.sqlite";
NSString * const appURLSchemeString = @"fpiPlace://";
