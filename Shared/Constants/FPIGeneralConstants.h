//
//  FPIGeneralConstants.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/8/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const appGroupIdentifier;
extern NSString * const sqliteStoreName;
extern NSString * const appURLSchemeString;
