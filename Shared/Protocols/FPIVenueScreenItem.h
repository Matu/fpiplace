//
//  FPIVenueScreenItem.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/8/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FPIVenueScreenItem <NSObject>

@required

@property (weak, nonatomic, readonly) NSString *displayName;
@property (weak, nonatomic, readonly) NSString *displayAddress;
@property (weak, nonatomic, readonly) NSURL *displayImageURL;

@end
