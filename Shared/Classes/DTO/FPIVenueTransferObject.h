//
//  FPIVenueTransferObject.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPIVenueScreenItem.h"

@class FPIVenueCoreDataObject;

@interface FPIVenueTransferObject : NSObject <FPIVenueScreenItem>

@property (copy, nonatomic, readonly) NSString *name;
@property (copy, nonatomic, readonly) NSString *address;
@property (copy, nonatomic, readonly) NSURL *imageURL;

- (instancetype)initWithName:(NSString *)name address:(NSString *)address imageURL:(NSURL *)url;
- (instancetype)initWithVenueCoreDataObject:(FPIVenueCoreDataObject *)venueCoreDataObject;

@end
