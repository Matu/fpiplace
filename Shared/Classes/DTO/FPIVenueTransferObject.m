//
//  FPIVenueTransferObject.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIVenueTransferObject.h"
#import "FPIVenueCoreDataObject+Extensions.h"

@interface FPIVenueTransferObject ()

@property (copy, nonatomic, readwrite) NSString *name;
@property (copy, nonatomic, readwrite) NSString *address;
@property (copy, nonatomic, readwrite) NSURL *imageURL;

@end

@implementation FPIVenueTransferObject

- (instancetype)initWithName:(NSString *)name address:(NSString *)address imageURL:(NSURL *)url
{
    NSParameterAssert(name.length);
    NSParameterAssert(address.length);
    
    if (!name.length || !address.length)
    {
        return nil;
    }
    
    self = [super init];
    
    if (self)
    {
        _name = name;
        _address = address;
        _imageURL = url;
    }
    
    return self;
}

- (instancetype)initWithVenueCoreDataObject:(FPIVenueCoreDataObject *)venueCoreDataObject
{
    self = [[FPIVenueTransferObject alloc] initWithName:venueCoreDataObject.name
                                                address:venueCoreDataObject.address
                                               imageURL:venueCoreDataObject.imageURL];
    
    return self;
}

#pragma mark - FPIVenueScreenItem

- (NSString *)displayName
{
    return self.name;
}

- (NSString *)displayAddress
{
    return self.address;
}

- (NSURL *)displayImageURL
{
    return self.imageURL;
}

@end
