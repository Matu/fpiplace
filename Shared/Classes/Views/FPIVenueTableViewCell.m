//
//  FPIVenueTableViewCell.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/7/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIVenueTableViewCell.h"
#import "FPIVenueScreenItem.h"

#import <SDWebImage/UIImageView+WebCache.h>

NSString * const fpiVenueTableViewCellReuseIdentifier = @"venueTableViewCell";

@interface FPIVenueTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end

@implementation FPIVenueTableViewCell

+ (NSString *)cellReuseIdentifier
{
    return fpiVenueTableViewCellReuseIdentifier;
}

- (void)prepareForReuse
{
    self.iconImageView.image = nil;
    self.nameLabel.text = nil;
    self.addressLabel.text = nil;
    
    [super prepareForReuse];
}

#pragma mark - Actions

- (void)configureWithVenue:(id<FPIVenueScreenItem>)venue
{
    [self.iconImageView sd_setImageWithURL:venue.displayImageURL
                          placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    self.nameLabel.text = venue.displayName;
    self.addressLabel.text = venue.displayAddress;
}

@end
