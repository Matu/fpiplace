//
//  FPIVenueTableViewCell.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/7/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FPIVenueCoreDataObject;

@protocol FPIVenueScreenItem;

@interface FPIVenueTableViewCell : UITableViewCell

+ (NSString *)cellReuseIdentifier;

- (void)configureWithVenue:(id<FPIVenueScreenItem>)venue;

@end
