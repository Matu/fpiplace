//
//  FPIVenueCoreDataObject.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/7/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIVenueCoreDataObject.h"

@implementation FPIVenueCoreDataObject

@dynamic name;
@dynamic address;
@dynamic imageURLPath;

@end
