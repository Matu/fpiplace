//
//  FPIVenueCoreDataObject+Extensions.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/7/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIVenueCoreDataObject+Extensions.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation FPIVenueCoreDataObject (Extensions)

+ (NSFetchRequest *)venuesFetchRequest
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([self class])];
    
    NSSortDescriptor *createdAtSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    fetchRequest.sortDescriptors = @[createdAtSortDescriptor];
    
    return fetchRequest;
}

+ (NSFetchedResultsController *)venuesFRC
{
    return [[NSFetchedResultsController alloc] initWithFetchRequest:[FPIVenueCoreDataObject venuesFetchRequest]
                                               managedObjectContext:[NSManagedObjectContext MR_defaultContext]
                                                 sectionNameKeyPath:nil
                                                          cacheName:nil];
}

#pragma mark Accessors

- (NSURL *)imageURL
{
    return [NSURL URLWithString:self.imageURLPath];
}

- (void)setImageURL:(NSURL *)imageURL
{
    self.imageURLPath = imageURL.absoluteString;
}

#pragma mark - FPIVenueScreenItem

- (NSString *)displayName
{
    return self.name;
}

- (NSString *)displayAddress
{
    return self.displayName;
}

- (NSURL *)displayImageURL
{
    return [NSURL URLWithString:self.imageURLPath];
}

@end
