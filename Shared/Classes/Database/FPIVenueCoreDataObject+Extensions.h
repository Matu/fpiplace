//
//  FPIVenueCoreDataObject+Extensions.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/7/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIVenueCoreDataObject.h"
#import "FPIVenueScreenItem.h"

@interface FPIVenueCoreDataObject (Extensions) <FPIVenueScreenItem>

@property (weak, nonatomic) NSURL *imageURL;

+ (NSFetchRequest *)venuesFetchRequest;
+ (NSFetchedResultsController *)venuesFRC;

@end
