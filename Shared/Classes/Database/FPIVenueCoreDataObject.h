//
//  FPIVenueCoreDataObject.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/7/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface FPIVenueCoreDataObject : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * imageURLPath;

@end
