//
//  FPIVenueTransferObject+FPIVenueMessage.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/9/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIVenueTransferObject+FPIVenueMessage.h"

NSString * const fpiVenueMessageURLScheme = @"https";
NSString * const fpiVenueMessageURLHost = @"fpiPlace.com";
NSString * const fpiVenueMessageURLName = @"name";
NSString * const fpiVenueMessageURLAddress = @"address";
NSString * const fpiVenueMessageURLImagePath = @"imagePath";

@implementation FPIVenueTransferObject (FPIVenueMessage)

- (instancetype)initWithVenueMessageURL:(NSURL *)url
{
    NSParameterAssert(url);
    
    if (!url)
    {
        return nil;
    }
    
    __block NSString *name;
    __block NSString *address;
    __block NSString *imagePath;
    
    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:url
                                                resolvingAgainstBaseURL:NO];
    
    [urlComponents.queryItems enumerateObjectsUsingBlock:^(NSURLQueryItem * _Nonnull queryItem, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([queryItem.name isEqualToString:fpiVenueMessageURLName])
        {
            name = queryItem.value;
        }
        else if ([queryItem.name isEqualToString:fpiVenueMessageURLAddress])
        {
            address = queryItem.value;
        }
        else if ([queryItem.name isEqualToString:fpiVenueMessageURLImagePath])
        {
            imagePath = queryItem.value;
        }
    }];
    
    return [self initWithName:name
                      address:address
                     imageURL:[NSURL URLWithString:imagePath]];
}

- (NSURL *)fpiVenueMessageURL
{
    NSURLComponents *urlComponents = [[NSURLComponents alloc] init];
    
    urlComponents.scheme = fpiVenueMessageURLScheme;
    urlComponents.host = fpiVenueMessageURLHost;
    
    NSURLQueryItem *nameQueryItem = [NSURLQueryItem queryItemWithName:fpiVenueMessageURLName
                                                                value:self.displayName];
    
    NSURLQueryItem *addressQueryItem = [NSURLQueryItem queryItemWithName:fpiVenueMessageURLAddress
                                                                   value:self.displayAddress];
    
    NSURLQueryItem *imagePathQueryItem = [NSURLQueryItem queryItemWithName:fpiVenueMessageURLImagePath
                                                                     value:self.displayImageURL.absoluteString];
    
    urlComponents.queryItems = @[nameQueryItem, addressQueryItem, imagePathQueryItem];
    
    return urlComponents.URL;
}

@end
