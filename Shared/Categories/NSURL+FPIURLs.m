//
//  NSURL+FPIURLs.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/8/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "NSURL+FPIURLs.h"
#import "FPIGeneralConstants.h"

@implementation NSURL (FPIURLs)

+ (instancetype)fpiSqliteStoreURL
{
    NSURL *storeURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:appGroupIdentifier];
    
    storeURL = [storeURL URLByAppendingPathComponent:sqliteStoreName];
    
    return storeURL;
}

+ (instancetype)fpiAppSchemeURL
{
    return [NSURL URLWithString:appURLSchemeString];
}

@end
