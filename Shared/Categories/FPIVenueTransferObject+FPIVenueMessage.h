//
//  FPIVenueTransferObject+FPIVenueMessage.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/9/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIVenueTransferObject.h"

@interface FPIVenueTransferObject (FPIVenueMessage)

- (instancetype)initWithVenueMessageURL:(NSURL *)url;
- (NSURL *)fpiVenueMessageURL;

@end
