//
//  NSURL+FPIURLs.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/8/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (FPIURLs)

+ (instancetype)fpiSqliteStoreURL;
+ (instancetype)fpiAppSchemeURL;

@end
