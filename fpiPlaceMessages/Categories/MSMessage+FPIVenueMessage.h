//
//  MSMessage+FPIVenueMessage.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/9/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <Messages/Messages.h>

@class FPIVenueTransferObject;

@interface MSMessage (FPIVenueMessage)

- (instancetype)initWithVenue:(FPIVenueTransferObject *)venue session:(MSSession *)session;

@end
