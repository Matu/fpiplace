//
//  MSMessage+FPIVenueMessage.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/9/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "MSMessage+FPIVenueMessage.h"
#import "FPIVenueTransferObject+FPIVenueMessage.h"
#import <SDWebImage/SDWebImageManager.h>

@implementation MSMessage (FPIVenueMessage)

- (instancetype)initWithVenue:(FPIVenueTransferObject *)venue session:(MSSession *)session
{
    NSParameterAssert(venue);
    
    if (!venue)
    {
        return nil;
    }
    
    self = [self initWithSession:session];
    
    if (self)
    {
        self.URL = [venue fpiVenueMessageURL];
        
        MSMessageTemplateLayout *layout = [[MSMessageTemplateLayout alloc] init];
        
        layout.image = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:[[SDWebImageManager sharedManager] cacheKeyForURL:venue.displayImageURL]];
        layout.caption = venue.displayName;
        
        self.layout = layout;
    }
    
    return self;
}

@end
