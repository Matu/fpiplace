//
//  FPIMessageViewController.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Messages/Messages.h>

@interface FPIMessageViewController : MSMessagesAppViewController

@end
