//
//  FPIMessageViewController.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIMessageViewController.h"
#import "NSURL+FPIURLs.h"
#import "FPIVenueTableViewCell.h"
#import "FPIVenueCoreDataObject+Extensions.h"
#import "FPIVenueTransferObject.h"
#import "MSMessage+FPIVenueMessage.h"
#import <MagicalRecord/MagicalRecord.h>
#import <SDWebImage/SDWebImageManager.h>
#import "FPIVenueTransferObject+FPIVenueMessage.h"

@interface FPIMessageViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy, nonatomic) NSArray <FPIVenueTransferObject *> *venuesDataSource;

@end

@implementation FPIMessageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreAtURL:[NSURL fpiSqliteStoreURL]];
    
    NSArray <FPIVenueCoreDataObject *> *venuesCoreDataObjects = [FPIVenueCoreDataObject MR_executeFetchRequest:[FPIVenueCoreDataObject venuesFetchRequest]
                                                                          inContext:[NSManagedObjectContext MR_defaultContext]];
    
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:venuesCoreDataObjects.count];
    
    [venuesCoreDataObjects enumerateObjectsUsingBlock:^(FPIVenueCoreDataObject * _Nonnull venueCoreDataObject, NSUInteger idx, BOOL * _Nonnull stop) {
        
        FPIVenueTransferObject *venue = [[FPIVenueTransferObject alloc] initWithVenueCoreDataObject:venueCoreDataObject];
        
        if (venue)
        {
            [result addObject:venue];
        }
    }];
    
    self.venuesDataSource = [NSArray arrayWithArray:result];
}

- (void)didSelectMessage:(MSMessage *)message conversation:(MSConversation *)conversation
{
    [super didSelectMessage:message
               conversation:conversation];
    
    if (message)
    {
        [self openFPIPlaceApplicationWithVenueMessageURL:message.URL];
    }
}

#pragma mark - Actions

- (IBAction)onSearchInFPIPlacePressed:(id)sender
{
    [self openFPIPlaceApplication];
}

- (void)openFPIPlaceApplication
{
    [self openFPIPlaceApplicationWithVenueMessageURL:nil];
}

- (void)openFPIPlaceApplicationWithVenueMessageURL:(NSURL *)venueMessageURL
{
    NSURL *url = [NSURL fpiAppSchemeURL];
    
    if (venueMessageURL)
    {
        url = [NSURL URLWithString:[url.absoluteString stringByAppendingString:venueMessageURL.absoluteString]];
    }
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url
                                           options:@{}
                                 completionHandler:nil];
    }
}

#pragma mark - UITableViewDataSource / UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.venuesDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FPIVenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[FPIVenueTableViewCell cellReuseIdentifier]
                                                                  forIndexPath:indexPath];
    
    [cell configureWithVenue:self.venuesDataSource[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
    MSMessage *message = [[MSMessage alloc] initWithVenue:self.venuesDataSource[indexPath.row]
                                                  session:self.activeConversation.selectedMessage.session];
    
    [self.activeConversation insertMessage:message
                         completionHandler:nil];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    
    UITableView *tableView = self.tableView;
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            
            [((FPIVenueTableViewCell *)[tableView cellForRowAtIndexPath:indexPath]) configureWithVenue:controller.fetchedObjects[indexPath.row]];
            break;
            
        case NSFetchedResultsChangeMove:
            
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
