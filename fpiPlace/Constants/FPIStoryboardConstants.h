//
//  FPIStoryboardConstants.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/7/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const showVenueDetailsViewControllerSegueIdentifier;
