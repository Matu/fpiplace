//
//  FPIStoryboardConstants.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/7/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIStoryboardConstants.h"

NSString * const showVenueDetailsViewControllerSegueIdentifier = @"showVenueDetailsViewController";
