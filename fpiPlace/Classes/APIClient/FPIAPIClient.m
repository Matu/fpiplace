//
//  FPIAPIClient.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIAPIClient.h"
#import <AFNetworking/AFNetworking.h>
#import <MagicalRecord/MagicalRecord.h>
#import "FPIResponseBuilder.h"
#import "FPIVenueTransferObject.h"
#import "FPIVenueCoreDataObject+Extensions.h"

NSString * const baseURLString = @"https://api.foursquare.com/v2/";
NSString * const foursquareClientId = @"V2AY3YORUWWP5RETWTJEWE5KIGHVP41DQ1IASC0HTNYBE11C";
NSString * const foursquareClientSecret = @"HDTHEKGNJBOOLBETEUMTOQMDG53EU3ICDEKMF2FFAIKJCAXL";
NSString * const venuesEndpoint = @"venues/explore";

@interface FPIAPIClient ()

@property (strong, nonatomic) AFHTTPSessionManager *httpSessionManager;
@property (strong, nonatomic) FPIResponseBuilder *responseBuilder;

@end

@implementation FPIAPIClient

+ (instancetype)sharedClient
{
    static FPIAPIClient *client;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        client = [[FPIAPIClient alloc] init];
    });
    
    return client;
}

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _httpSessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString]
                                                       sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        _httpSessionManager.requestSerializer.timeoutInterval = 30.0f;
        
        _responseBuilder = [[FPIResponseBuilder alloc] init];
    }
    
    return self;
}

#pragma mark - Actions

- (void)getAndCacheVenuesForLatitude:(double)latitude longitude:(double)longitude offset:(int)offset limit:(int)limit completion:(FPIAPIClientSuccessErrorCompletion)completion
{
    FPIAPIClientSuccessErrorCompletion completionCopy = [completion copy];
    
    NSString *endpoint = [NSString stringWithFormat:@"%@?ll=%f,%f&section-food&limit=%i&offset=%i&client_id=%@&client_secret=%@&v=20161006&venuePhotos=1", venuesEndpoint, latitude, longitude, limit, offset, foursquareClientId, foursquareClientSecret];
    
    __weak typeof(self) weakSelf = self;
    
    [self.httpSessionManager GET:endpoint
                      parameters:nil
                        progress:nil
                         success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary *responseObject) {
                         
                             [weakSelf.responseBuilder buildVenuesFromResponse:responseObject[@"response"]
                                                                withCompletion:^(NSArray<FPIVenueTransferObject *> *result, NSError *error) {
                                                                    
                                                                    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                                                                        
                                                                        NSArray *oldVenues = [FPIVenueCoreDataObject MR_findAllInContext:localContext];
                                                                        
                                                                        [oldVenues enumerateObjectsUsingBlock:^(FPIVenueCoreDataObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                                                            
                                                                            [localContext deleteObject:obj];
                                                                        }];
                                                                    }
                                                                                      completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                                                                         
                                                                                          [result enumerateObjectsUsingBlock:^(FPIVenueTransferObject * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                                                                              
                                                                                              [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                                                                                                  
                                                                                                  FPIVenueCoreDataObject *venue = [FPIVenueCoreDataObject MR_createEntityInContext:localContext];
                                                                                                  
                                                                                                  venue.name = obj.name;
                                                                                                  venue.address = obj.address;
                                                                                                  venue.imageURL = obj.imageURL;
                                                                                              }
                                                                                                                completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                                                                                                                    
                                                                                                                    if (completionCopy)
                                                                                                                    {
                                                                                                                        completionCopy(YES, nil);
                                                                                                                    }
                                                                                                                }];
                                                                                          }];
                                                                     }];
                                                                }];
                         }
                         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                             
                             if (completionCopy)
                             {
                                 completionCopy(NO, error);
                             }
                         }];
}

@end
