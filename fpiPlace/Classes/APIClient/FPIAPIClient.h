//
//  FPIAPIClient.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^FPIAPIClientSuccessErrorCompletion)(BOOL success, NSError *error);

@interface FPIAPIClient : NSObject

+ (instancetype)sharedClient;

- (void)getAndCacheVenuesForLatitude:(double)latitude longitude:(double)longitude offset:(int)offset limit:(int)limit completion:(FPIAPIClientSuccessErrorCompletion)completion;

@end
