//
//  FPIHomeViewController.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIHomeViewController.h"
#import "FPIAPIClient.h"
#import "FPIVenueTransferObject.h"
#import "FPIVenueCoreDataObject+Extensions.h"
#import "FPIVenueTableViewCell.h"
#import "FPIDetailsViewController.h"
#import "FPIStoryboardConstants.h"
#import "AppDelegate.h"

@interface FPIHomeViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *venuesFRC;

@end

@implementation FPIHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.venuesFRC = [FPIVenueCoreDataObject venuesFRC];
    self.venuesFRC.delegate = self;
    [self.venuesFRC performFetch:nil];
    
    [[FPIAPIClient sharedClient] getAndCacheVenuesForLatitude:51.50
                                                    longitude:-0.07
                                                       offset:0
                                                        limit:5
                                                   completion:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (((AppDelegate *)[UIApplication sharedApplication].delegate).venueFromLaunch)
    {
        [self performSegueWithIdentifier:showVenueDetailsViewControllerSegueIdentifier
                                  sender:((AppDelegate *)[UIApplication sharedApplication].delegate).venueFromLaunch];
        
        ((AppDelegate *)[UIApplication sharedApplication].delegate).venueFromLaunch = nil;
    }
}

#pragma mark - Navigation
 
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:showVenueDetailsViewControllerSegueIdentifier])
    {
        ((FPIDetailsViewController *)segue.destinationViewController).venue = sender;
    }
}

#pragma mark - UITableViewDataSource / UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.venuesFRC.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FPIVenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[FPIVenueTableViewCell cellReuseIdentifier]
                                                                  forIndexPath:indexPath];
    
    [cell configureWithVenue:self.venuesFRC.fetchedObjects[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
    [self performSegueWithIdentifier:showVenueDetailsViewControllerSegueIdentifier
                              sender:[[FPIVenueTransferObject alloc] initWithVenueCoreDataObject:self.venuesFRC.fetchedObjects[indexPath.row]]];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    
    UITableView *tableView = self.tableView;
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            
            [((FPIVenueTableViewCell *)[tableView cellForRowAtIndexPath:indexPath]) configureWithVenue:controller.fetchedObjects[indexPath.row]];
            break;
            
        case NSFetchedResultsChangeMove:
            
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
