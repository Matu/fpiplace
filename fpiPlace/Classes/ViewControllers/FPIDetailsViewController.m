//
//  FPIDetailsViewController.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIDetailsViewController.h"
#import "FPIVenueTransferObject.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface FPIDetailsViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end

@implementation FPIDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.venue.name;
    
    [self.coverImageView sd_setImageWithURL:self.venue.imageURL
                           placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.nameLabel.text = self.venue.name;
    self.addressLabel.text = self.venue.address;
}

@end
