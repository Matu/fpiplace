//
//  FPIResponseBuilder.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIResponseBuilder.h"

#import "FPIVenueTransferObject.h"
#import "FPIVenueResponseBuildOperation.h"

@interface FPIResponseBuilder ()

@property (strong, nonatomic) NSOperationQueue *responseBuilderQueue;

@end

@implementation FPIResponseBuilder

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _responseBuilderQueue = [[NSOperationQueue alloc] init];
    }
    
    return self;
}

#pragma mark - Actions

- (void)buildVenuesFromResponse:(NSDictionary *)responseObject withCompletion:(FPIResponseBuilderCompletion)completion
{
    FPIResponseBuilderCompletion completionCopy = [completion copy];
    
    FPIVenueResponseBuildOperation *operation = [[FPIVenueResponseBuildOperation alloc] initWithVenueResponse:responseObject
                                                                                                   completion:^(NSArray<FPIVenueTransferObject *> *result, NSError *error) {
                                                                                                       
                                                                                                       if (completionCopy)
                                                                                                       {
                                                                                                           completionCopy(result, error);
                                                                                                       }
                                                                                                   }];
    
    if (operation)
    {
        [self.responseBuilderQueue addOperation:operation];
    }
}

@end
