//
//  FPIVenueResponseBuildOperation.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FPIVenueTransferObject;

typedef void(^FPIVenueResponseBuildOperationCompletion)(NSArray<FPIVenueTransferObject *> *result, NSError *error);

@interface FPIVenueResponseBuildOperation : NSOperation

- (instancetype)initWithVenueResponse:(NSDictionary *)responseObject completion:(FPIVenueResponseBuildOperationCompletion)completion;

@end
