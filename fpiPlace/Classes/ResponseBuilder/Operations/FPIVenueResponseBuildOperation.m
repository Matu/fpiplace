//
//  FPIVenueResponseBuildOperation.m
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import "FPIVenueResponseBuildOperation.h"
#import "FPIVenueTransferObject.h"

@interface FPIVenueResponseBuildOperation ()

@property (copy, nonatomic) NSDictionary *responseObject;
@property (copy, nonatomic) FPIVenueResponseBuildOperationCompletion operationCompletion;

@end

@implementation FPIVenueResponseBuildOperation

- (instancetype)initWithVenueResponse:(NSDictionary *)responseObject completion:(FPIVenueResponseBuildOperationCompletion)completion
{
    NSParameterAssert(responseObject);
    
    if (!responseObject)
    {
        return nil;
    }
    
    self = [super init];
    
    if (self)
    {
        _responseObject = responseObject;
        _operationCompletion = completion;
    }
    
    return self;
}

- (void)main
{
    NSArray *groups = self.responseObject[@"groups"];
    
    __block NSDictionary * recommendedGroup;
    
    [groups enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj[@"name"] isEqualToString:@"recommended"])
        {
            recommendedGroup = obj;
            *stop = YES;
        }
    }];
    
    NSMutableArray *resultArr = [NSMutableArray array];
    
    [recommendedGroup[@"items"] enumerateObjectsUsingBlock:^(NSDictionary *venueObj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        venueObj = venueObj[@"venue"];
        FPIVenueTransferObject *venue = [[FPIVenueTransferObject alloc] initWithName:venueObj[@"name"]
                                                                             address:[NSString stringWithFormat:@"%@ %@", venueObj[@"location"][@"city"], venueObj[@"location"][@"address"]]
                                                                            imageURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@300x300%@", [venueObj[@"featuredPhotos"][@"items"] firstObject][@"prefix"], [venueObj[@"featuredPhotos"][@"items"] firstObject][@"suffix"]]]];
        
        [resultArr addObject:venue];
    }];
    
    if (self.operationCompletion)
    {
        self.operationCompletion([NSArray arrayWithArray:resultArr], nil);
    }
}

@end
