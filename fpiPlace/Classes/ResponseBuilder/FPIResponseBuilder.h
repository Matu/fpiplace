//
//  FPIResponseBuilder.h
//  fpiPlace
//
//  Created by Yaroslava Mazepina on 10/6/16.
//  Copyright © 2016 Daniil Mazepin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^FPIResponseBuilderCompletion)(id result, NSError *error);

@interface FPIResponseBuilder : NSObject

- (void)buildVenuesFromResponse:(NSDictionary *)responseObject withCompletion:(FPIResponseBuilderCompletion)completion;

@end
